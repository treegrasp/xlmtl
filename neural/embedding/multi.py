from typing import List


from torch import concat, Tensor
from torch.nn import Module


from .charemb import CharEmb
from .fasttext import FastText


class MultiEmb(Module):

    def __init__(self, config, vocabularies, device):
        super().__init__()
        self.config = config
        self._embedding_dim = 0
        for emb_name, emb_config in config.items():
            column_idx = emb_config['column_idx']
            vocabulary = vocabularies[column_idx]
            if emb_config['type'] == 'char':
                self.add_module(emb_name, CharEmb(
                    vocabulary,
                    emb_config['char_size'],
                    emb_config['size'],
                    emb_config['dropout'],
                    device,
                ))
                self._embedding_dim += emb_config['size']
            elif emb_config['type'] == 'fasttext':
                emb = FastText(
                    emb_config['path'],
                    dropout=emb_config['dropout'],
                )
                self.add_module(emb_name, emb)
                self._embedding_dim += emb.embedding_dim()
            else:
                raise ValueError(
                    f'unsupported embedding type: {emb_config["type"]}',
                )

    def forward(self, word: List[str]) -> Tensor:
        '''One-word variant of forwards'''
        return self.forwards([word])[0]

    def forwards(self, inputs: List[List[str]]) -> Tensor:
        outputs = []
        for emb_name, emb_config in self.config.items():
            emb = getattr(self, emb_name)
            column_idx = emb_config['column_idx']
            outputs.append(emb.forwards(inputs[column_idx]))
        return concat(outputs, dim=1)

    def embedding_dim(self):
        return self._embedding_dim
