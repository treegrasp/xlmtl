from typing import Set, List

import torch
import torch.nn as nn
from torch import Tensor
import torch.nn.utils.rnn as rnn

from xlmtl.neural.encoding import Encoding


class CharEmb(nn.Module):
    '''Character-based word embedding module.'''
    def __init__(
        self,
        words: Set[str],        # the set of words in the training set
        char_embedding_dim: int,
        word_embedding_dim: int,
        dropout: float = 0,
        device = 'cpu'
    ):
        super(CharEmb, self).__init__()
        self.chars = set(c for word in words for c in word)
        self.char_enc = Encoding(self.chars)
        self.char_emb = emb = nn.Embedding(
            num_embeddings=self.char_enc.size()+1,
            embedding_dim=char_embedding_dim,
            padding_idx=self.char_enc.size()
        )
        self.char_lstm = MapLSTM(
            inp_size=char_embedding_dim,
            out_size=word_embedding_dim
        )
        self.word_embedding_dim=word_embedding_dim
        self.dropout = nn.Dropout(p=dropout, inplace=False)
        self.device = device
        # TODO: Additionally optimize the model by using index cache
        # for entire words

    def encode_word(self, word: str) -> Tensor:
        '''Encode word as as 1d tensor of indices.'''
        # TODO: optimize by using caching?
        return torch.tensor(
            [self.char_enc.encode(c) for c in word]
        ).to(self.device)

    # TODO Why do we need this?? Why is forwards not called forward?
    def forward(self, word: str) -> Tensor:
        '''One-word variant of forwards'''
        return self.forwards([word])[0]

    def forwards(self, words: List[str]) -> Tensor:
        '''Embed several words at a time.'''
        # TODO: optimize!
        ixs = [self.char_emb(self.encode_word(word)) for word in words]
        return torch.stack([
            # hs[-1] contains the last output vector of the char-based LSTM
            # for the particular word in the sentence
            self.dropout(hs[-1])
            for hs in self.char_lstm(ixs)
        ])

    def embedding_dim(self) -> int:
        """Return the dimensionality of the embedding vectors."""
        return self.word_embedding_dim


class MapLSTM(nn.Module):
    """Variant of unidirectional LSTM which works with
    packed sequence representations.

    MapLSTM(...) is roughly equivalent to Map(SimpleLSTM(...)).

    Type: List[Tensor[N x Din]] -> List[Tensor[N x Dout]], where
    * `N` is is the length of an input sequence
    * `Din` is the input embedding size
    * `Dout` is the output embedding size

    Source: https://github.com/kawu/hhu-dl-materials-2020/tree/main/char
    """

    def __init__(self, inp_size: int, out_size: int, **kwargs):
        super().__init__()
        self.lstm = nn.LSTM(
            input_size=inp_size, hidden_size=out_size,
            bidirectional=False, **kwargs,
        )

    def forward(self, xs):
        '''Apply the LSTM to the input sentence.'''
        seq_inp = rnn.pack_sequence(xs, enforce_sorted=False)
        seq_out, _ = self.lstm(seq_inp)
        seq_unpacked, lens_unpacked = rnn.pad_packed_sequence(seq_out, batch_first=True)
        ys = []
        for sent, n in zip(seq_unpacked, lens_unpacked):
            ys.append(sent[:n])
        return ys
