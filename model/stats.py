from typing import Dict
from dataclasses import dataclass

from xlmtl.neural.proto import ScoreStats


"""Stats calculated when training the model"""


@dataclass
class AccStats(ScoreStats):
    """Accuracy statistics"""

    tp_num: int
    all_num: int

    def add(self, other):
        return AccStats(
            self.tp_num + other.tp_num,
            self.all_num + other.all_num
        )

    def acc(self):
        return 100 * self.tp_num / self.all_num

    def report(self):
        return f'{self.acc():2.2f}'


@dataclass
class MultiStats(ScoreStats):
    """Full statistics (joint model)"""

    stats: Dict[str, AccStats]

    def add(self, other: 'MultiStats') -> 'MultiStats':
        return MultiStats({
            n: self.stats[n].add(other.stats[n])
            for n in self.stats
        })

    def report(self):
        return '[' + ' '.join(f'{n}={p.acc():05.2f}' for n, p in self.stats.items()) + ']'
