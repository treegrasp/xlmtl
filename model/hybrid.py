import copy
import json
from typing import Dict, Iterable, List, Tuple


import numpy as np
import torch
from torch import Tensor
from torch.nn import Module, Parameter, Sequential
import torch.nn.utils.rnn as rnn
from torch.nn.utils.rnn import PackedSequence


from xlmtl.data import Sent, SentDist
from xlmtl.model.dep_parser import DepParser
from xlmtl.model.tagger import Tagger
from xlmtl.model.stats import AccStats, MultiStats
from xlmtl.model.utils import Context
from xlmtl.model.utils import Embed
from xlmtl.neural.embedding.multi import MultiEmb
from xlmtl.neural.proto import Neural
from xlmtl.neural.utils import unpack


"""Prediction

Represents a sentence combined with a map from predictor names to output
distributions.
"""
Pred = Tuple[Sent, Dict[str, Tensor]]


def sent2srcsent(sent: Sent, config: Dict) -> Sent:
    """Extract the source sentence from a Sent"""
    column_idx = config['column_idx']
    sents = [json.loads(s) for s in sent[column_idx]]
    unique = set(tuple(tuple(l) for l in s[1:]) for s in sents)
    assert len(unique) == 1 # only one aligned sentence supported
    sent = sents[0]
    sent = [l[1:] for l in sent] # remove dummy token
    sent[0] = list(range(1, len(sent) + 1)) # replace weights by token numbers
    return sent


def sent2wa(sent: Sent, config: Dict) -> Tensor:
    """Extract the alignment matrix from a Sent"""
    column_idx = config['column_idx']
    sents = [json.loads(s) for s in sent[column_idx]]
    wa = [s[0] for s in sents]
    return np.array(wa, dtype='float32')


class EmbedProjectDetach(Module):
    """Embedding/projection module.  The projected embeddings are detached from
    the computation graph.

    Type: Iterable[InpSent] -> PackedSequence
    """

    def __init__(self, embed: Module, config: Dict, size: int, device: str):
        """Create the embedding/alignment module.

        The given emedding should have type Iterable[List[str]] -> PackedSequence.
        """
        super().__init__()
        self.embed = embed
        self.config = config
        self.device=device
        # Representation of the dummy root (initially 0)
        self.root_repr = Parameter(torch.zeros(size).to(self.device))

    def forward(self, batch: Iterable[Sent]) -> PackedSequence:
        # Extract the word alignments and create the input for the embedding layer
        sents, was = [], []
        for inp in batch:
            sents.append(sent2srcsent(inp, self.config))
            was.append(sent2wa(inp, self.config))

        # Extract the embedding
        seq = self.embed(sents)

        # Unpack the embedded sequence
        unseq = unpack(seq)
        assert len(unseq) == len(was)
        assert len(unseq) > 0

        # # Create the dummy root representation
        # repr_size = unseq[0].shape[-1]
        # root_repr = torch.zeros(repr_size).to(self.device)

        # Apply the individual word alignment matrices
        outs = []
        for embs, wa in zip(unseq, was):
            # Detach the embeddings from the computation graph and
            # add the root representation
            embs_root = torch.cat([embs.detach(), self.root_repr.view(1, -1)])
            # print(f'Embs_root: {embs_root.device}; requires_grad={embs_root.requires_grad}')
            # Transform via the alignment matrix
            out = torch.mm(torch.tensor(wa).to(self.device), embs_root)
            outs.append(out)
        return rnn.pack_sequence(outs, enforce_sorted=False)


class CatPacked(Module):

    """An alternative to the Avg class which concatenates instead of
    calculating the averages values.
    """

    def __init__(self, *modules: Module):
        super().__init__()
        self.module_list = modules

    def forward(self, inp):
        out = [m(inp) for m in self.module_list]
        padded = [rnn.pad_packed_sequence(seq) for seq in out]

        # Sanity check
        for (unseq1, lengths1), (unseq2, lengths2) in zip(padded, padded[1:]):
            assert (lengths1 == lengths2).all()
            # At this point, both `unseq1` and `unseq2` are 3d L x B x D
            # tensors, where L = max(lengths), B is the batch size, and D is
            # the embedding size.  Note however that the embedding size D can
            # differ between `unseq1` and `unseq2`.
            assert unseq1.dim() == 3
            assert unseq1.shape[:2] == unseq2.shape[:2]

        # Concatenate along the last dimension
        unseqs = [u for u, l in padded]
        lengths = padded[0][1]
        unseq = torch.cat(unseqs, dim=-1)
        # unseq = unseq2 / 2 + unseq2 / 2

        out = rnn.pack_padded_sequence(unseq, lengths1, enforce_sorted=False)
        return out


class HybridModel(Module, Neural[Sent, Sent, Pred, MultiStats]):

    def __init__(self, config, vocabularies, src_model, device):
        super().__init__()
        # Store config and vocabularies
        self.config = config
        self.vocabularies = vocabularies
        # Input embedding layers
        self.embed = Embed(
            MultiEmb(config['embeddings'], vocabularies, device),
            device=device,
        )
        # First contextualization layer
        self.embed = Sequential(
            self.embed,
            Context(config['context']),
        )
        # Task-specific representations from source model
        src_tag_embed = EmbedProjectDetach(
            src_model.tag_embed,
            config['twin'],
            src_model.tag_embed_size(),
            device=device,
        )
        src_dep_embed = EmbedProjectDetach(
            src_model.dep_embed,
            config['twin'],
            src_model.dep_embed_size(),
            device=device,
        )
        # Task-specific contextualization layers
        self.tag_embed = Sequential(
            CatPacked(src_tag_embed, self.embed),
            Context(config['tag_context']),
        )
        self.dep_embed = Sequential(
            CatPacked(src_dep_embed, self.embed),
            Context(config['dep_context']),
        )
        # Prediction layers
        for pred_name, pred_conf in config['predictors'].items():
            column_idx = pred_conf['column_idx']
            vocabulary = vocabularies[column_idx]
            if pred_conf['type'] == 'tagger':
                self.add_module(pred_name, Tagger(
                    pred_conf,
                    vocabulary,
                    self.tag_embed,
                    device=device,
                ))
            elif pred_conf['type'] == 'dep_parser':
                self.add_module(pred_name, DepParser(
                    pred_conf,
                    self.dep_embed,
                    device=device,
                ))
            else:
                raise ValueError(
                    f'unsupported predictor type: {pred_conf["type"]}'
                )
        # Internal state for round-robin training
        self.step = 0
        # Move to device
        self.to(device)

    def forward(self, inp: Sent) -> Pred:
        return inp, {
            pred_name: getattr(self, pred_name)(inp)
            for pred_name in self.config['predictors'].keys()
        }

    def decode(self, pred: Pred) -> Sent:
        sent, predictions = pred
        sent = copy.deepcopy(sent)
        for pred_name, pred_config in self.config['predictors'].items():
            prediction = predictions[pred_name]
            predictor = getattr(self, pred_name)
            decoded = predictor.decode(prediction)
            column_idx = pred_config['column_idx']
            sent[column_idx] = decoded
        return sent

    def decode_dist(self, pred: Pred, nbest: int) -> SentDist:
        """Variant of `dist` which returns distributions"""
        sent, predictions = pred
        sent = copy.deepcopy(sent)
        for pred_name, pred_config in self.config['predictors'].items():
            prediction = predictions[pred_name]
            predictor = getattr(self, pred_name)
            decoded = predictor.decode_dist(prediction, nbest)
            column_idx = pred_config['column_idx']
            sent[column_idx] = decoded
        return sent

    def loss(self, batch: List[Tuple[Sent, Sent]]) -> Tensor:
        pred_names = list(self.config['predictors'].keys())
        k = self.step
        self.step = (self.step + 1) % len(pred_names)
        pred_name = pred_names[k]
        pred_conf = self.config['predictors'][pred_name]
        column_idx = pred_conf['column_idx']
        predictor = getattr(self, pred_name)
        inps = [x for x, _ in batch]
        gold = [y[column_idx] for _, y in batch]
        pred_type = pred_conf['type']
        if pred_type == 'dep_parser':
            gold = [[int(x) for x in sent] for sent in gold]
        return predictor.loss(list(zip(inps, gold, strict=True)))

    def score(self, golds: Sent, preds: Sent) -> MultiStats:
        all_num = len(golds[0])
        stats = {}
        for pred_name, pred_config in self.config['predictors'].items():
            tp_num = 0
            column_idx = pred_config['column_idx']
            for pred, gold in zip(preds[column_idx], golds[column_idx], strict=True):
                if str(pred) == str(gold):
                    tp_num += 1
            stats[pred_name] = AccStats(tp_num=tp_num, all_num=all_num)
        return MultiStats(stats)

    def module(self):
        return self

    def save(self, path):
        state = {
            'config': self.config,
            'vocabularies': self.vocabularies,
            'state_dict': self.state_dict(),
        }
        torch.save(state, path)

    @staticmethod
    def load(path, src_model, device) -> 'HybridModel':
        # load model state
        state = torch.load(path, map_location=device)
        # create new model with config
        model = HybridModel(
            config=state['config'],
            vocabularies=state['vocabularies'],
            src_model=src_model,
            device=device,
        )
        model.load_state_dict(state['state_dict'])
        return model
