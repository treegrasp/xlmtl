import copy
import sys
from typing import Dict, List, Tuple


import torch
from torch import Tensor
from torch.nn import Module, Sequential


from xlmtl.data import Sent, SentDist
from xlmtl.model.dep_parser import DepParser
from xlmtl.model.tagger import Tagger
from xlmtl.model.stats import AccStats, MultiStats
from xlmtl.model.utils import Context
from xlmtl.model.utils import Embed
from xlmtl.neural.embedding.multi import MultiEmb
from xlmtl.neural.proto import Neural


"""Prediction

Represents a sentence combined with a map from predictor names to output
distributions.
"""
Pred = Tuple[Sent, Dict[str, Tensor]]


class RoundRobin(Module, Neural[Sent, Sent, Pred, MultiStats]):

    def __init__(self, config, vocabularies, device):
        super().__init__()
        # Store config and vocabularies
        self.config = config
        self.vocabularies = vocabularies
        # Input embedding layers
        self.embed = Embed(
            MultiEmb(config['embeddings'], vocabularies, device),
            device=device,
        )
        # First contextualization layer
        self.embed = Sequential(
            self.embed,
            Context(config['context']),
        )
        # Task-specific contextualization layers
        self.tag_embed = Sequential(
            self.embed,
            Context(config['tag_context']),
        )
        self.dep_embed = Sequential(
            self.embed,
            Context(config['dep_context']),
        )
        # Prediction layers
        for pred_name, pred_conf in config['predictors'].items():
            column_idx = pred_conf['column_idx']
            vocabulary = vocabularies[column_idx]
            if pred_conf['type'] == 'tagger':
                self.add_module(pred_name, Tagger(
                    pred_conf,
                    vocabulary,
                    self.tag_embed,
                    device=device,
                ))
            elif pred_conf['type'] == 'dep_parser':
                self.add_module(pred_name, DepParser(
                    pred_conf,
                    self.dep_embed,
                    device=device,
                ))
            else:
                raise ValueError(
                    f'unsupported predictor type: {pred_conf["type"]}'
                )
        # Internal state for round-robin training
        self.step = 0
        # Move to device
        self.to(device)

    def tag_embed_size(self) -> int:
        return self.config['tag_context']['out_size'] * 2

    def dep_embed_size(self) -> int:
        return self.config['dep_context']['out_size'] * 2

    def forward(self, inp: Sent) -> Pred:
        return inp, {
            pred_name: getattr(self, pred_name)(inp)
            for pred_name in self.config['predictors'].keys()
        }

    def decode(self, pred: Pred) -> Sent:
        sent, predictions = pred
        sent = copy.deepcopy(sent)
        for pred_name, pred_config in self.config['predictors'].items():
            prediction = predictions[pred_name]
            predictor = getattr(self, pred_name)
            decoded = predictor.decode(prediction)
            column_idx = pred_config['column_idx']
            sent[column_idx] = decoded
        return sent

    def decode_dist(self, pred: Pred, nbest: int) -> SentDist:
        """Variant of `dist` which returns distributions"""
        sent, predictions = pred
        sent = copy.deepcopy(sent)
        for pred_name, pred_config in self.config['predictors'].items():
            prediction = predictions[pred_name]
            predictor = getattr(self, pred_name)
            decoded = predictor.decode_dist(prediction, nbest)
            column_idx = pred_config['column_idx']
            sent[column_idx] = decoded
        return sent

    def loss(self, batch: List[Tuple[Sent, Sent]]) -> Tensor:
        pred_names = list(self.config['predictors'].keys())
        k = self.step
        self.step = (self.step + 1) % len(pred_names)
        pred_name = pred_names[k]
        pred_conf = self.config['predictors'][pred_name]
        column_idx = pred_conf['column_idx']
        predictor = getattr(self, pred_name)
        inps = [x for x, _ in batch]
        gold = [y[column_idx] for _, y in batch]
        pred_type = pred_conf['type']
        if pred_type == 'dep_parser':
            gold = [[int(x) for x in sent] for sent in gold]
        return predictor.loss(list(zip(inps, gold, strict=True)))

    def score(self, golds: Sent, preds: Sent) -> MultiStats:
        all_num = len(golds[0])
        stats = {}
        for pred_name, pred_config in self.config['predictors'].items():
            tp_num = 0
            column_idx = pred_config['column_idx']
            for pred, gold in zip(preds[column_idx], golds[column_idx], strict=True):
                if str(pred) == str(gold):
                    tp_num += 1
            stats[pred_name] = AccStats(tp_num=tp_num, all_num=all_num)
        return MultiStats(stats)

    def module(self):
        return self

    def save(self, path):
        state = {
            'config': self.config,
            'vocabularies': self.vocabularies,
            'state_dict': self.state_dict(),
        }
        torch.save(state, path)

    @staticmethod
    def load(path, device) -> 'RoundRobin':
        # load model state
        state = torch.load(path, map_location=device)
        # create new model with config
        model = RoundRobin(
            config=state['config'],
            vocabularies=state['vocabularies'],
            device=device,
        )
        model.load_state_dict(state['state_dict'])
        return model
