import sys
from typing import Iterable, TextIO, Tuple


def read(f: TextIO=sys.stdin) -> Iterable[Tuple[str, ...]]:
    current_block = []
    for line in f:
        if line.strip() == '':
            yield tuple(current_block)
            current_block = []
        else:
            current_block.append(line)
    if current_block:
        yield tuple(current_block)


def write(block: Iterable[str], f: TextIO=sys.stdout):
    for line in block:
        print(line, end='', file=f)
    print(file=f)
