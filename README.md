xlmtl
=====

Cross-lingual multi-task learning

About
-----

This is a work-in-progress toolkit for sequence labeling with multiple outputs
(e.g., POS tags, dependency heads, supertags, semantic roles) and multiple
inputs (e.g., character embeddings, pretrained embeddings, morphological
feature embeddings, cross-lingually projected embeddings). Both inputs and
outputs are provided in [MaChAmp](https://github.com/machamp-nlp/machamp)-style
column-based files, with the main difference that there can be multiple input
columns (not just plain tokens).

An example project (cross-lingual RRG parsing) is available at
https://gitlab.com/treegrasp/xlmtl-rrg.

Credits
-------

Code based on [rrgproj2](https://gitlab.com/treegrasp/rrgproj2) by Jakub
Waszczuk and Kilian Evang.
