from io import FileIO
from typing import Dict, List, Union


from xlmtl.util import blocks


"""A list of layers

Each layer is a sentence-length list of strings such as token, pos, gloss,
supertag, dependency head.

We don't distinguish between input and output, this is done in configuration."""
Sent = List[List[str]]


"""A list of layers with n-best predictions"""
SentDist = List[List[Union[str, Dict[str, float]]]]


def read(f: FileIO) -> List[Sent]:
    result = []
    for block in blocks.read(f):
        result.append(
            list(zip(*(l.strip().split('\t') for l in block), strict=True)),
        )
    return result


def write(s: SentDist):
    rows = zip(*s)
    for row in rows:
        print('\t'.join(render_field(f) for f in row))
    print()


def render_field(f: Union[str, Dict[str, float]]) -> str:
    if isinstance(f, dict):
        return '|'.join(f'{k}:{v}' for k, v in f.items())
    return f
